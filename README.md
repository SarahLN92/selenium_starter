# Selenium_starter

# The Basics
First open VS and select ‘Windows desktop(?)’ under the templates sidebar and choose ‘Console Application’, then give your scripts a name below and check the file path.
To get up and running in FireFox I downloaded the NuGet packages:

Selenium WebDriver (usually comes up as first result)—— You’ll only need this once I am sure
And Selenium.FireFox.WebDriver 

Note: If you attempt to run the project without installing the server set up packages for FF, IE, Chrome etc you will get a ’Service Not Found’ unhandled error.
Similar things must be done for IE and Chrome. I went to the Selenium page and downloaded the servers there because that’s how I saw those done. 



```
In the code: 
 IWebDriver driver = new ChromeDriver(@"C:\libraries\");
You want to initialize the driver as above. 
Ultimately where I ended up with a simple set up is the following: 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
namespace WDDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver(@"C:\libraries\");
            driver.Url = "http://google.com";

          var searchBox =  driver.FindElement(By.ClassName("gLFyf"));
            searchBox.SendKeys("pluralsight");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            var imageLink = driver.FindElement(By.LinkText("Images"));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            imageLink.Click();
        }
    }
}
```

What’s nice if you look at this code is a lot of the syntax, though unique to Selenium, is pretty readable and self explanatory. 

# Creating A FrameWork
I’ve gone and started a new project in VS. I’ve started a Class Project. Within the Project that I’ve named TestFramework I created a UnitTest by right clicking and saying Add-> Unit Test (I did not select xUnit Test). We want the test framework to be dependent on Selenium and we want the tests themselves to be dependent on the test framework.
 Knowing this I go into the Test-> References and right click I say add reference and I select ’TestFramework’ as a ref and hit okay. I then go to the references inside the TestFrameWork here I add a NuGet package and it’s, you guessed it, Selenium Webdriver and Selenium support classes. 
